import java.io.IOException;
import java.util.Scanner;
		
public class TicTacToe {
		
	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
		
		printBoard(board);
		int count = 0;
		boolean gate = true;
		int row;
		int col;
		boolean draw = true;

		while (true) {
			if (gate) {
				System.out.print("Player 1 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 1 enter column number:");
				col = reader.nextInt();
				if ( row <= 3 && row > 0 && col <= 3 && col > 0) {
					if (emptyControl(board, row, col)) board[row - 1][col - 1] = 'X';
					else {
						System.out.println("Player 1, it was occupied. Please enter again!");
						continue;}
				} else {
					System.out.println("Player 1, it was out of range. Please enter again!");
					continue;
				}
				printBoard(board);
				if (checkboard(board)) {
					System.out.println("Player 1 won!");
					draw = false;
					break;
				}
			}
		
			count = 0; 
			for (char[] x : board) {
				for (char y : x) {
					if (y == ' ') count++; 
				}
			}
			if (count == 0) break;
	
			System.out.print("Player 2 enter row number:");
			row = reader.nextInt();
			System.out.print("Player 2 enter column number:");
			col = reader.nextInt();
			if (row <= 3 && row > 0 && col <= 3 && col > 0) {
				if (emptyControl(board, row, col)) board[row - 1][col - 1] = 'O';
				else {
					System.out.println("Player 2, it was occupied. Please enter again!");
					gate = false;
					continue;}
			} else {
				System.out.println("Player 2, it was out of range. Please enter again!");
				gate = false;
				continue;
			}
			printBoard(board);
			if (checkboard(board)) {
				System.out.println("Player 2 won!");
				draw = false;
				break;
			}
			gate = true;
		}
		if (draw) System.out.println("Game draw!");
		
		reader.close();
	}	

	public static boolean checkboard(char[][] board) {
		//for rows
		for (int i = 0; i < 3; i++) {
			if (board[i][0] != ' ' && board[i][0] == board[i][1] && board[i][1] == board[i][2]) return true; //for rows
			else if (board[0][i] != ' ' && board[0][i] == board[1][i] && board[1][i] == board[2][i]) return true; //for columns
			else if (board[0][0] != ' ' && board[0][0] == board[1][1] && board[1][1] == board[2][2]) return true; // from upper left to bottom right diagnol
		       	else if (board[0][2] != ' ' && board[0][2] == board[1][1] && board[1][1] == board[2][0]) return true; // from upper right to bottom left diagnol	
		}
		return false;
	}
		
	public static boolean emptyControl(char[][] board, int row, int col) {
		if (board[row-1][col-1]== ' ') return true;
	       	return false;	
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");
			
			}
			System.out.println();
			System.out.println("   -----------");
			
		}
		
	}	
		
}		
